/* SIDHARTH JHA 10032687 CPSC 355 Assignment5 Part A (m file) */

/* Function prototypes to be coded:
	void push(int value); 	//takes an integer input and pushes it onto the top of the stack
	int pop(); 					//removes the element at the top of the stack and returns the removed element
	int stackFull();			//returns TRUE (i.e 1) if the number of elements on the stack is equal to its capacity, FALSE otherwise
	int stackEmpty();		//returns TRUE (i.e. 1) if there are no elements on the stack, FALSE otherwise
	void display(); 			//returns the element at the top of the stack
*/

include(macro_defs.m)
define(STACKSIZE,	5)		!define the capacity of the stack
define(FALSE,	0)				!define a False integer
define(TRUE,		1)				!define a True integer
define(top_r,		l0)			!define top_r into register %l0
define(topValue_r,	l1)		!define topValue_r into register %l1
define(i_r,			l2)			!define a counter i_r into register %l2

.global		top_m,	stack_m						!global variables
.global		push,	pop,	stackFull,	display	!defining the function prototypes

.section	".data"					!data section
.align		4	
top_m:						
	.word	-1							!top initialized to -1
	.align	4
stack_m:	
	.skip		4	*	STACKSIZE	!allocate space for the global stack 4*5 = 20 
	.align	4

/* Print formats defined for the print call*/
stackFullPrint:		!To print the message indicating a full stack and can't push
	.asciz	"\nStack overflow! Cannot push value onto stack. \n"
	.align	4
stackEmptyPrint:	!To print the message when the stack is empty and can't pop
	.asciz	"\nStack underflow! Cannot pop an empty stack. \n"
	.align	4
emptyStack:			!To print the message when the stack is empty
	.asciz	"\nEmpty stack\n"
	.align	4			
stackLabel:			!To print the heading for the display operation
	.asciz	"\nCurrent stack contents: \n"
	.align	4
contentString:		!To print the appropriate values on the stack (stack[i])
	.asciz	"	%d"
	.align	4
newLine:				!To add a new line appropriately
	.asciz	"\n"
	.align	4
topString:				!To print the message indicating the top of the stack
	.asciz	" <-- top of stack"
	.align	4

.section	".text"	!text section

/* Begin push subroutine */
push:
	save		%sp,			-96,				%sp
	sethi		%hi(top_m),						%o0		!retrieve the value from top_m
	ld			[%o0	+	%lo(top_m)],		%o1		!and load it into the top register
	mov		%o1,		%top_r							!copy the current value of top_m into the top register
	call		stackFull					!call stackFull subroutine to check and return the current status of whether the stack is full (i.e 0 or 1)
	nop
	cmp		%o0,		%g0			!if result is 0
	be		elsePush					!branch to elsePush and push the element
	nop
	set		stackFullPrint,	%o0	!set print string for stack overflow
	call		printf							!and print
	nop
	ba 		donePush					!branch to end of subroutine
	nop
	
	elsePush:
		set	top_m,			%o1				!set the pointer of top_m into %o1
		set	stack_m,		%o2				!set the pointer of stack_m into %o2
		inc	%top_r								!increment the value of top (top = top +1)
		st		%top_r,			[%o1]			!store the new value into %o1 thus updating top_m
		sll		%top_r,			2,			%o0	!%o0 = top*4
		add	%o2,			%o0,	%o2	!add top_r to the stack to point to the right location to store the new value
		st		%i0,				[%o2]			!store the new value into the location
	donePush:
		ret
		restore

/* Begin pop subroutine */
pop:
	save		%sp,			-96,				%sp
	sethi		%hi(top_m),						%o0		!retrieve the value from top_m
	ld			[%o0	+	%lo(top_m)],		%o1		!and load it into the top register
	mov		%o1,	%top_r								!copy current value of top_m into the top register
	call		stackEmpty					!call stackEmpty leaf
	nop
	cmp		%o0,	%g0					!if result is false
	be		elsePop						!branch to elsePop and pop the element
	nop
	set		stackEmptyPrint,	%o0	!set print string for stack underflow
	call		printf								!and print
	nop
	ba		donePop						!branch to end of subroutine
	mov		-1,	%i0
	
	elsePop:
		set		stack_m,			%o1	!set the pointer of stack_m into %o1
		sll			%top_r,		2,		%o2	!%o2 = top*4
		add		%o1,	%o2,	%o1	!add top_r to the stack to point to the right location to load the top value
		ld			[%o1],	%topValue_r	!load the top value from the stack into the topValue register
		set		top_m,	%o0				!set the top_m pointer into %o0
		dec		%top_r						!decrement top index register by 1
		st			%top_r,	[%o0]			!store the new decremented top index into top_m
		mov		%topValue_r,	%i0	!return the value at the top
	donePop:
		ret
		restore

/* Begin stackFull subroutine */		
stackFull:
	save		%sp,			-96,				%sp
	sethi		%hi(top_m),						%o0		!retrieve the value from top_m
	ld			[%o0	+	%lo(top_m)],		%o1		!and load it into the top register
	mov		STACKSIZE,		%l3
	sub		%l3,					1,				%l5		!capacity of the stack - 1 to get the maximum index of the elements
	cmp		%o1,				%l5						!compare top to stacksize-1 = 4
	bne 		notFull											!and branch to notFull if they are not equal
	nop
	mov		TRUE,				%i0						!move 1 into %i0 to return
	ba		doneStackFull								!and branch to doneStackFull to end current subroutine
	nop											
	notFull:														
		mov	FALSE,			%i0						!move 0 into %i0 to return
	doneStackFull:											
		ret														!returns to the address at %i7+8 (i.e. the next operation after the delay slot of the subroutine from which the call was made)
		restore

/* Begin stackEmpty Subroutine */
stackEmpty:
	save		%sp,			-96,					%sp
	sethi		%hi(top_m),	%o0								!retrieve the value from top_m
	ld			[%o0	+		%lo(top_m)],		%o1		!and load it into the top register
	mov		-1,				%o2								!move -1 into %o2 for comparison
	cmp		%o1,			%o2								!compare current top to -1
	bne		notEmpty											!and branch to notEmpty if they aren't equal
	nop	
	mov		TRUE,			%i0								!move 1 into %i0 to return
	ba		doneStackEmpty								!branch to doneStackFull to end current subroutine
	nop
	notEmpty:
		mov	FALSE,		%i0								!move 0 into %i0 to return
	doneStackEmpty:
		ret															!returns to the address at %i7+8 (i.e. the next operation after the delay slot of the subroutine from which the call was made)
		restore

/*	Begin display subroutine */
display:
	save		%sp,			-96,				%sp
	sethi		%hi(top_m),						%o0		!retrieve the value from top_m
	ld			[%o0	+	%lo(top_m)],		%o1		!and load it into the top register
	mov		%o1,	%top_r								!copy current value of top_m into the top register
	call		stackEmpty									!call stackEmpty to check if the stack is currently empty (result in %o0)
	nop
	cmp		%o0,	%g0									!if result is false
	be		elseDisplay									!branch to elseDisplay and display the contents of the stack
	nop
	set		emptyStack,	%o0							!set print string for empty stack
	call		printf												!and print
	nop
	ba doneDisplay											!branch to doneDIsplay to end current subroutine
	nop
	
	elseDisplay:
		set	stackLabel,	%o0							!set the heading to display the contents
		call	printf												!and print
		nop
		mov	%top_r,			%i_r							!i = top
		
		loop:														!begin loop
			cmp	%i_r,	%g0								!compare counter i and 0
			bl		doneDisplay								!branch to doneDisplay if i<0 and end the subroutine
			nop
			set	stack_m,			%o2					!set the pointer for stack into %o2
			sll		%i_r,	2,			%o3					!i = i*4
			add	%o3,	%o2,	%o3					!add that to the pointer and resulting pointer goes in %o3
			ld		[%o3],	%o1								!then load the value from that pointer into %o1 (i.e. stack[i])
			set	contentString,	%o0					!set the format to display stack[i]
			call	printf											!and print
			nop
			cmp	%i_r,		%top_r						!compare counter i and top
			bne	endBody									!i != top then branch to endBody
			nop					
			set	topString,	%o0							!set the string that prints the message indicating the top value of the stack
			call	printf											!and print
			nop
			endBody:
				set	newLine,	%o0						!string to go to the next line
				call	printf										!and call printf
				nop
				dec	%i_r										!decrement counter i by 1 (i.e. i = i - 1)
				ba	loop										!branch always back to loop beginning
				nop
				
doneDisplay:
	ret
	restore
