/* SIDHARTH JHA 10032687 CPSC 355 Assignment5 Part B (m file) */

/* Program accepts as commands line arguments three integer
strings representing a date in the format mm dd yyyy. The program
will print the date with the name of month as well as the correct suffix. */

include(macro_defs.m)
day_TH:		!the print format to include 'th' after the appropriate day entered (and print format for month and year too)
	.asciz		"%s  %dth,  %d\n"
	.align		4
day_ST:		!the print format to include 'st' after the appropriate day entered (and print format for month and year too)
	.asciz		"%s  %dst,  %d\n"
	.align		4
day_ND:		!the print format to include 'nd' after the appropriate day entered (and print format for month and year too)
	.asciz		"%s  %dnd,  %d\n"
	.align		4
day_RD:		!the print format to include 'rd' after the appropriate day entered (and print format for month and year too)
	.asciz		"%s  %drd,  %d\n"
	.align		4
errorString:	!the print string format for the error
	.asciz		"usage mm dd yyyy\n"
	.align		4

/* define array of months */

	.global		month
month:	.word	jan_m,	feb_m,	mar_m,	apr_m,	may_m,	jun_m	
			.word	jul_m,	aug_m,	sep_m,	oct_m,	nov_m,		dec_m

	jan_m:		.asciz	"January"
	feb_m:		.asciz	"February"
	mar_m:		.asciz	"March"
	apr_m:		.asciz	"April"
	may_m:	.asciz	"May"
	jun_m:		.asciz	"June"
	jul_m:		.asciz	"July"
	aug_m:		.asciz	"August"
	sep_m:		.asciz	"September"
	oct_m:		.asciz	"October"
	nov_m:		.asciz	"November"
	dec_m:		.asciz	"December"
	.align		4

/* Begin main */

	.global main
main:	
	save	%sp,	-96,		%sp
	
	cmp	%i0,		4							!compare the number of arguments in the command line to 4
	bne	terminate							!and branch to terminate if they are not equal
	nop	
	
	/* 	get month into register l0	 */
	ld		[%i1	+	4],		%o0			!grab month argument (i.e. second argument from command line)
	call 	atoi									!convert the string to and integer
	nop
	sub	%o0,	1,			%o0			!subtract 1 from the integer and store result in %o0
	sll		%o0,	2,			%o0			!month index * 4
	set	month,	%o1						!set the month pointer in %o1
	add	%o1,	%o0,	%o1			!add the appropriate value to the pointer to get it at the right month
	ld		[%o1],	%l0						!load that month
	/* 	get day into register l1 	*/
	ld		[%i1	+	8],		%o0			!grab day argument (i.e. third from command line)
	call 	atoi									!convert the string to and integer
	nop
	mov	%o0,			%l1				!move result in %l1
	/* 	get year into register l2	 */
	ld		[%i1	+	12],		%o0			!grab year argument (i.e. third from command line)
	call 	atoi									!convert the string to and integer
	nop
	mov	%o0,			%l2				!move result in %l2
	/* 	possible cases for the day argument	 */
	cmp	%l1,		1		!case 1 where day is 1
	be	firstDay
	nop
	cmp	%l1,		2		!case 2 where day is 2
	be	secondDay
	nop
	cmp	%l1,		3		!case 3	where day is 3
	be	thirdDay
	nop
	cmp	%l1,		4		!case 4 where day >= 4
	bge	fourthDay
	nop
	/* branches */
	firstDay:
		set	day_ST,	%o0		!print format for day_ST (3 parameters)
		mov	%l0,			%o1		!month into %o1
		mov	%l1,			%o2		!day into %o2
		mov	%l2,			%o3		!year into %o3
		call	printf
		nop
		ba done						!branch to done thus ending the program
		nop
	secondDay:						
		set	day_ND,	%o0		!print format for day_ND (3 parameters month, day and year)
		mov	%l0,			%o1		!month into %o1
		mov	%l1,			%o2		!day into %o2
		mov	%l2,			%o3		!year into %o3
		call	printf						!print it
		nop
		ba	done						!branch to done thus ending the program
		nop
	thirdDay:
		set	day_RD,	%o0		!print format for day_RD (3 parameters)
		mov	%l0,			%o1		!month into %o1
		mov	%l1,			%o2		!day into %o2
		mov	%l2,			%o3		!year into %o3
		call	printf						!print it
		nop
		ba	done						!branch to done thus ending the program
		nop
	fourthDay:
		cmp	%l1,			11		!compare the day to 11
		be	firstDay					!and if equal, branch to firstDay to ultimately print (with st appended to the day)
		nop
		cmp	%l1,			21		!compare the day to 21
		be	firstDay					!and if equal, branch to firstDay to ultimately print (with st appended to the day)
		nop
		cmp	%l1,			31		!compare the day to 31
		be	firstDay					!and if equal, branch to firstDay to ultimately print (with st appended to the day)
		nop
		cmp	%l1,			22		!compare the day to 22
		be	secondDay			!and if equal, branch to secondDay to ultimately print (with nd appended to the day)
		nop
		cmp	%l1,			23		!compare the day to 23
		be	thirdDay				!and if equal, branch to secondDay to ultimately print (with rd appended to the day)
		nop
		/* 	rest of the days apart
		from the ones that satisfy the checks
		above will have th appended to them 	*/
		set	day_TH,	%o0		!print format for day_TH
		mov	%l0,			%o1		!month into %o1
		mov	%l1,			%o2		!day into %o2
		mov	%l2,			%o3		!year into %o3
		call	printf						!print
		nop
		ba	done
		nop
terminate:
	set	errorString,	%o0		!set the errorString to %o0 for printing
	call	printf							!and print
	nop
done:
	mov	1,		%g1
	ta		0




