/* SIDHARTH JHA 10032687 CPSC 355 Assignment4 */
include(macro_defs.m)

/****		FALSE and TRUE are used later to return a result via the equal subroutine		****/
define(FALSE,	0) !define FALSE to be 0
define(TRUE,		1)	!define TRUE to be 1

/****			Structure: 'point defined'				****/
begin_struct(point)
	field(x,	4)			!define the size of point_x (int) to be 4 
	field(y,	4)			!define the size of point_x (int) to be 4 
end_struct(point)

/****			Structure: 'dimension' defined		****/
begin_struct(dimension)
	field(width,		4)	!define the size of dimension_width (int) to be 4
	field(height,	4)	!define the size of dimension_height (int) to be 4
end_struct(dimension)

/****			Structure: 'box' defined				****/
begin_struct(box)
	field(origin,	align_of_point,			size_of_point)			!define nested structure box_origin
	field(size,		align_of_dimension,	size_of_dimension)	!define nested structure box_dimension
	field(area,		4)																!define size for box_area
end_struct(box)

/*		initialize local variables for first and second box		*/
local_var
	var(result, 4)													!make space for result on the stack
	var(first,			align_of_box,		size_of_box)		!make space for first box
	var(second,	align_of_box,		size_of_box)		!make space for second box
	var(b,			align_of_box,		size_of_box)		!make space for box b

/****		Format of the output strings to be displayed		****/
stringB:				!string to output the values of the box
	.asciz	"Box %s origin = (%d, %d)  width = %d  height = %d  "
	.align	4
stringArea:		!split from stringB because of a shortage of o-registers
	.asciz	"area = %d\n"
	.align	4
stringFirst:		!label for the first box
	.asciz	"first\n"
	.align	4
stringSecond:	!label for the second box
	.asciz	"second\n"
	.align	4
stringInitial:		!label for intial values of the box
	.asciz	"Initial box values:\n"
	.align	4
stringFinal:		!label for the changed values of the box
	.asciz	"\nChanged box values:\n"
	.align	4

/*----	Start subroutine: newBox
args passed: N/A
return type: box b	----*/
	.global	newBox
newBox:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	mov		0,			%l0																!move 0 into register %l0
	ld			[%fp	+	struct_s],	%o0												!pointer is set into %o0
	st			%l0,		[%o0	+	box_origin	+	point_x]					!store 0 into point x for the box (origin)
	st			%l0,		[%o0	+	box_origin	+	point_y]					!store 0 into point y for the box (origin)
	mov		1,			%g2																!move 1 into %g2
	st			%g2,	[%o0	+	box_size	+	dimension_width]		!store 1 into the width for the dimension
	st			%g2,	[%o0	+	box_size	+	dimension_height]		!store 1 into the height for the dimension
	ld			[%o0	+	box_size	+	dimension_width],			%l1	!load the width into register %l1
	ld			[%o0	+	box_size	+	dimension_height],			%l2	!load the height into register %l2
	smul		%l1,		%l2,		%l3													!move the result of width x height into %l3
	st			%l3,		[%o0	+	box_area]										!store %l4 into the area space on the stack
ret
restore

/*----	Start subroutine: move
args passed: struct 'first' or 'second', int deltaX , int deltaY
return type: void	----*/
.global	move
move:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	ld		[%i0	+	box_origin	+	point_x],	%l0					!load the box origin value (point x) into register %l0
	ld		[%i0	+	box_origin	+	point_y],	%l1					!load the box origin value (point y) into register %l1
	
	add	%l0,		%i1,		%l0											!add point x value (%l0) for the box and deltaX (%i1) into %l0
	st		%l0,		[%i0	+	box_origin	+	point_x]				!store the result (%l0) on the stack (point x)
	add	%l1,		%i2,		%l1											!add point y value (%l1) of the box and deltaY (%i2) into %l1
	st		%l1,		[%i0	+	box_origin	+	point_y]				!store the result (%l1) on the stack (point y)
ret
restore

/*----	Start subroutine: expand
args passed: struct box 'first' or 'second', int factor
return type: void	----*/
.global	expand
expand:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	ld			[%i0	+	box_size	+	dimension_width],	%l0		!load box dimension (width value) into %l0
	smul		%l0,		%i1,		%l0											!multiply %l0 and %i1 (factor to expand by) into %l0
	st			%l0,		[%i0	+	box_size	+	dimension_width]	!store the resultant value (%l0) on the stack space for the width
	
	ld			[%i0	+	box_size	+	dimension_height],	%l1		!load box dimension (height value) into %l1
	smul		%l1,		%i1,		%l1											!multiply %l1 and %i1 (factor to expand by) into %l1
	st			%l1,		[%i0	+	box_size	+	dimension_height]	!store the resultant value (%l1) on the stack space for the height
	
	smul		%l0,		%l1,		%l2											!multiply the new width and the new height into %l2
	st			%l2,		[%i0	+	box_area]									!store the value onto the stack space for the area
ret
restore

/*----	Start subroutine: printBox
args passed: char *name, struct box 'first' or 'second'
return type: void	----*/
.global	printBox
printBox:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	ld		[%fp + struct_s],	%l0												!pointer is set into %o0
	ld		[%l0	+	box_origin	+	point_x],						%o2		!store point x value into %o2
	ld		[%l0	+	box_origin	+	point_y],						%o3		!store point y value into %o3
	ld		[%l0	+	box_size	+	dimension_width],			%o4		!store dimension width value into %o4
	ld		[%l0	+	box_size	+	dimension_height],			%o5		!store dimension height value into %o5
	set	stringB,						%o0											!set the print statement stringB into %o0
	call	printf																			!call printf
	mov	%i0,							%o1											!move the name of the box (%i0) into register %o1
	
	set	stringArea,				%o0											!set the print statement stringArea into %o0		
	call 	printf																			!call printf
	ld		[%l0	+	box_area],		%o1											!load the area value from the stack into %o1
ret
restore

/*----	Begin subroutine: equal
args passed: struct box 'first', struct box 'second'
return type: result (true or false)	----*/
.global	equal
equal:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	ld		[%fp	+	result],		%g3												!pointer for result it set into %g3
	mov	FALSE,				%g3												!set %g3 to 0
	
	ld		[%i0	+	box_origin	+	point_x],				%l0				!load point x for the first box into register %l0
	ld		[%i0	+	box_origin	+	point_y],				%l1				!load point y for the first box into register %l1
	ld		[%i0	+	box_size	+	dimension_width],	%l2				!load width for the first box into register %l2
	ld		[%i0	+	box_size	+	dimension_height],	%l3				!load height for the first box into register %l3
	ld		[%i1	+	box_origin	+	point_x],				%l4				!load point x for the second box into register %l4
	ld		[%i1	+	box_origin	+	point_y],				%l5				!load point y for the second box into register %l5
	ld		[%i1	+	box_size	+	dimension_width],	%l6				!load width for the second box into register %l6
	ld		[%i1	+	box_size	+	dimension_height],	%l7				!load height for the second box into register %l7
	
	/* branches to out if any of the two values compared are not equal */
	cmp	%l0,	%l4		!compare point x for the first and the second box
	bne	unequal
	cmp	%l1,	%l5		!compare point y for the first and the second box
	bne	unequal
	cmp	%l2,	%l6		!compare width for the first and the second box
	bne	unequal
	cmp	%l3,	%l7		!compare height for the first and the second box
	bne	unequal
	nop						!necessary nop
	mov	TRUE,	%g3	!move 1 into %g3 when all the properties of the first box are equal to the second box
unequal:
	st		%g3,	[%fp	+	result]	!store the true or false result (1 or 0) into the stack space for result
	ld		[%fp	+	result],	%o0		!load the result into %o0 to be returned
ret
restore

/*----	main	----*/
.global	main
main:	save	%sp,	(-92	+	last_sym)	&	-8,	%sp
	add	%fp,		first,			%o0							!set %o0 to point to the first box					
	call	newBox												!call the newBox subroutine to initialize the first box values
	st		%o0, 	[%sp + struct_s]						!store the pointer %o0
	add	%fp,		second,	%o0							!set %o0 to point to the second box
	call	newBox												!call the newBox subroutine to initialize the second box values
	st		%o0, 	[%sp + struct_s]						!store the pointer %o0
	
	set	stringInitial,	%o0									!set the label in stringInitial into %o0
	call	printf														!call printf
	nop																!necessary nop
	set	stringFirst,	%o0									!set stringFirst into %o0
	add	%fp, 	first,	%o1									!set %o1 to point to the first box
	call 	printBox												!call printBox subroutine to print the initial values of the first box
	st		%o1, 	[%sp + struct_s]						!store the pointer %o1
	
	set	stringSecond,		%o0							!set stringSecond into %o0
	add	%fp, 	second,	%o1							!set %o1 to point to the second box
	call 	printBox												!call printBox subroutine to print the initial values of the second box
	st		%o1, 	[%sp + struct_s]						!store the pointer %o1
	
	add	%fp,		first,			%o0							!set %o0 to point to the first box
	add	%fp,		second,	%o1							!set %o0 to point to the second box
	call	equal													!call equal subroutine to check if the first and second box properties match
	st		%o0,					[%sp	+	result]		!store the result returned from the equal subroutine into the stack space for result
	ld		[%sp	+	result],	%l1							!load the result value (1 or 0) into %l1
	
	mov		1,		%l2											!move 1 into %l2
	cmp		%l2,	%l1											!compare %l2 and the result value in %l1
	bne		finalPrint											!branch to finalPrint if they are not equal
	mov		-7,			%o2									!move -7 (int deltaY) into %o2
	add		%fp,	first,	%o0									!set %o0 to point to the first box
	call 		move												!call the move subroutine to move the box
	mov		-5,			%o1									!move -5 (int deltaX) into %o1
	
	add		%fp,			second,	%o0					!set %o0 to point to the second box
	call 		expand												!call expand subroutine to expand the second box
	mov		3,				%o1									!move 3 (int factor) into %o1
finalPrint:
	set	stringFinal,	%o0									!set stringFinal into %o0
	call 	printf														!call printf
	nop																!necessary nop
	set	stringFirst,	%o0									!set stringFirst into %o0
	add	%fp, 	first,	%o1									!set %o1 to point to the first box
	call 	printBox												!call printBox subroutine to print the final contents of the first box
	st		%o1, 	[%sp + struct_s]						!store the pointer %o1
	set	stringSecond,		%o0							!set stringSecond into %o0
	add	%fp, 	second,	%o1							!set %o1 to point to the second box
	call 	printBox												!call printBox subroutine to print the final contents of the second box
	st		%o1, 	[%sp + struct_s]						!store the pointer %o1
mov	1,	%g1
ta	0
