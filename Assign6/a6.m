/* SIDHARTH JHA 10032687 CPSC 355 Assignment6 a6.m */

/* Brief description of my approach in general:
	1. Subroutine to calculate e^x
	2. Subroutine to calculate e^(-x)
	3. Helper subroutines:
		-factorial (returns calculated factorial of a given value)
		-exponent subroutine to calculate the exponent for each term in the series
		-term checker subroutine to combine numerator and denominator (i.e. the result of exponent and factorial respectively)
		and check if the calculated term is less than the given range (1.0e-10),
		then it shall return the calculated term and a boolean value 1 or 0 depending of whether it passed the range check
		into the calling subroutine.
*/

include(macro_defs.m)

/* variables defined */
define(ex_fr, f20)		!to store the result of e^x
define(range_fr, f22)	!to define the range condition of the last accumulaed term
define(enx_fr,	f24)	!to store the result of e^(-x)

define(READ,		3)	!Read instruction
define(OPEN,		5)	!Open instruction

define(BUFFERSIZE,	8)	!size of double precision (buffer)
define(TRUE,		1)	!true = 1
define(FALSE,		0)	!false = 0
define(ff_r,		l5)	!variable to store the file descriptor
define(n_r,			l2)	!%n_r = %l2
define(i_r,			l1)	!%i_r is a counter to track the position in the series


/* print formats */
.section	".rodata"
			.align	4
header:			!print format of the column headers
	.asciz	"\nX value		 e^(x)			     e^(-x)\n"
errorStatement:	!print format for the error statement which is printed when the filename argument is not supplied
	.asciz	"\nError: Please specify a filename parameter\n"
	.align	8
finalPrint:		!print format for e^(-x)
	.asciz	"%28.10f\n"
	.align	8
inputX:			!print format for Input value (x) and e^(x)
	.asciz	"%.2f\t %.10f"


/* global data used primarily between floating point subroutines */
.section	".data"
			.align	4
	range:	
			.double	0r1.0e-10	!define range to store the max range value for the calculated terms
	zero:	
			.double	0r0.0		!to store type double 0.0
	one:	
			.double	0r1.0		!to store type double 1.0
	nOne:
			.double	0r-1.0		!to store type double -1.0
	num:	.double	0r0.0		!space to store the result of the exponent calculation (i.e. numerator of the respective term of type double)
	denom:	.double	0r0.0		!space to store the result of the factorial (type double)
	term:	.double	0r0.0		!space to store the result of termChecker subroutine (i.e. the next calculated term in the series)
	x:		.double	0r0.0		!space to store the x value (read from the file) to be passed into eX or eNegX subRoutine
	result:	.double	0r0.0		!space to store the result of eX or eNegX
	
	
/* define routine and subroutines */
.section	".text"
			.align	4

/*** START eX SUBROUTINE ***/
begin_fn(eX)
	set		zero,	%o0			!set the pointer of zero into %o0
	ldd		[%o0],	%f0			!%f0 initialized to 0.0 (i.e. both %f0 && %f1 => 0)
	set		one,	%o0			!set the pointer of one into %o0
	ldd		[%o0],	%ex_fr		!%ex_fr initialized to contain the result (i.e. %ex_fr = both %f20 & %f21 => 1.0 + accumulated terms)
	set		nOne,	%o0			!set the pointer of nOne (-1.0) into %o0
	ldd		[%o0],	%f18		!%f18 && %f19 => -1.0
	set		range,	%o0			!set the pointer for range into %o0
	ldd		[%o0],	%f10		!%f10 && %f11 => 1.0e-10
	set		x,		%o0			!set the pointer for x into %o0
	ldd		[%o0],	%f4			!%f4 && %f5 => x value
	
	/*case 1 when x is 0*/
	fcmpd	%f4,	%f0			!compare x to 0.0
	nop
	fbe		endSeries			!and if equal, branch to endSeries
	nop
	mov		1,	%i_r			!move 1 into %i_r
	accumulate:							!accmulates terms in the series e^x
		mov		%i_r,	%o0				!exponent of x term (i.e. num) into %o0
		call	exponent				!get the exponent
		nop
		mov		%i_r,	%o0				!denom to find the factorial value of, into %o0
		call	factorial				!and find the factorial value
		nop
		call	termChecker				!call termChecker which returns true or false and stores the term (i.e. term = num/denom) into memory for global access
		nop
		cmp		%o1,	%g0				!compare the boolean true or false (i.e. 1 or 0 respectively) returned by termChecker to 0
		be		endSeries				!if false is returned then branch to endSeries to return the result
		nop
		set		term,	%o0				!set the pointer of currently stored term into %o0
		ldd		[%o0],	%f6				!%f6 && %f7 => the term to be added
		faddd	%ex_fr,	%f6,	%ex_fr	!%ex_fr => %ex_fr + %f6 (in double precision)
		inc		%i_r					!i = i + 1
		ba		accumulate				!always branch back to accumulate
		nop
	crashSeries:	/* returns -1 when the range of the float double precision has been exceeded during calculation (i.e. >E308) */
		set	nOne,	%o0		!set the pointer for nOne into %o0
		ldd	[%o0],	%f18	!%f18 && %f19 => -1.0
		set	result,	%o0		!set the pointer for the result into %o0
		std	%f18,	[%o0]	!%o0 => result in %f18 && %f19 (stored onto memory)
		ba	doneEX			!always branch to doneEX
		nop
	endSeries:		
		cmp		%i_r,	171	!compare counter position to 171 (i.e. maximum factorial that can be computed to the capacity of double precision float, namely 10^308)
		bge		crashSeries	!if it is equal or exceeded, branch to crashSeries
		nop
		set	result,	%o0		!set the pointer for the result into %o0
		std	%ex_fr,	[%o0]	!store the result of e^x into %o0 (i.e. result on memory for global access)
	doneEX:
		end_fn(eX)


/*** START eX SUBROUTINE ***/
/*  returns e^(-x) which is equal to 1/e^x
	and follows all the conditions of the program
	as the absolute value of the last accumulated term
	in the series should be greater than or equal to the
	range of 1E-1. This would be the same term for both
	the series mathematically hence we can just reciprocate
	the result of e^x to find the correct result of the series
	expansion of e^(-x) as well.
*/
begin_fn(eNegX)
	set		result,	%o0				!set the pointer for result into %o0
	ldd		[%o0],	%enx_fr			!load the result of e^x into %enx_fr
	set		one,	%o0				!set the pointer for one into %o0
	ldd		[%o0],	%f2				!%f2 && %f3 => 1.0
	fdivd	%f2,	%enx_fr,	%f6	!%f6 = %f2/%enx_fr ( i.e. 1/(e^x) )
	set		result,	%o0				!set the pointer for the result into %o0
	std		%f6,	[%o0]			!store %f6 (&& %f7) into %o0 (i.e. store the result of e^(-x) into memory)
	doneENegX:
		end_fn(eNegX)


/***--HELPER SUBROUTINES FOR eX AND eNegX--***/
local_var
	var(tempfr,	4)
/*--CALCULATES FACTORIAL--*/
begin_fn(factorial)
	st	%i0,	[%fp	+	tempfr]		!store the counter value onto variable tempfr on stack
	ld	[%fp	+	tempfr],	%f2		!%f2 => counter in integer format
	set	one,	%o0						!set the pointer for one into %o0
	ldd	[%o0],	%f0						!%f0 (&& %f1) => 1.0
	ldd	[%o0],	%f4						!%f4 (&& %f5) => 1.0
	ldd	[%o0],	%f6						!%f6 (&& %f7) => 1.0
	fitod	%f2,	%f2					!convert %f2 to double precision
	loopFac:
		fcmpd	%f4,	%f2				!compare %f4 to %f2
		nop
		fbg		doneFactorial			!if it is greater than, branch to doneFactorial
		nop
		fmuld	%f0,	%f4,	%f0		!%f0 => %f4 * %f0
		faddd	%f4,	%f6,	%f4		!%f4 => %f4 + %f6
		ba	loopFac						!always branch to loopFac
		nop
	doneFactorial:
		set		denom,	%o0				!set the pointer for denom into %o0
		std		%f0,	[%o0]			!move factorial result into denom
		end_fn(factorial)

/*--CALCULATES EXPONENT--*/
begin_fn(exponent)
	clr	%o0					!clear register %o0					
	mov		%i0,	%l0		!move i into %l0
	mov		1,		%l1		!move 1 into %l1
	set		x,		%o0		!set the pointer for the current x value into %o0
	ldd		[%o0],	%f6		!%f6 (&& %f7) => x
	fabss	%f6,	%f0		!take the absolute value of x and move it into %f0
	fmovs	%f7,	%f1		!&& the lower register into %f1 appropriately
	set		one,	%o0		!set the pointer for one into %o0
	ldd		[%o0],	%f2		!%f2 && %f3 => (double)1.0
	fcmpd	%f0,	%f2		!compare abs(x) to 1.0
	nop
	fbe		outExp			!if equal, branch to outExp
	nop
	loopExp:
		cmp		%l1,	%l0			!compare %l1 and the counter i
		bg		outExp				!if greater, branch to outExp
		nop
		fmuld	%f2,	%f0,	%f2	!%f2 contains the updated numerator value (x multiplied by itself as many times as necessary)
		inc		%l1					!increment %l1 (i.e. the counter)
		ba		loopExp				!always branch to loopExp
		nop
	outExp:						
		set		num,	%o0			!set the pointer for num into %o0
		std		%f2,	[%o0]		!and store the result of the calculation (i.e. the numerator of the term) into %f2
	doneExponent:
		end_fn(exponent)

/*--TERMCHECKER--*/
begin_fn(termChecker)
	set		num,	%o0			!set the pointer for num into %o0
	ldd		[%o0],	%f0			!%f0 (&& %f1) => num
	set		denom,	%o0			!set the pointer for denom into %o0
	ldd		[%o0],	%f2			!%f2 (&& %f30) => denom
	fdivd	%f0,	%f2,	%f4	!%f4 = %f0/%f2 (term = num/denom)
	set		term,	%o0			!set the pointer for the term into %o0
	std		%f4,	[%o0]		!store the term into memory
	set		range,	%o0			!set the pointer for range into %o0
	ldd		[%o0],	%range_fr	!%range_fr => 1.0e-10
	fabss	%f4,	%f6			!take the absolute value of the calculated term
	fmovs	%f5,	%f7			!move the lower part of the double
	fcmpd	%f6,	%range_fr	!compare abs(term) to the range
	nop	
	fbul	fl					!branch if unordered or less than range to fl
	nop
	mov		TRUE,	%i1			!move TRUE into %i1
	ba		doneTermChecker		!branch always to doneTermChecker
	nop
	fl:
		mov	FALSE,	%i1			!move FALSE itno %i1
	doneTermChecker:
		end_fn(termChecker)


/*** START MAIN ROUTINE ***/
local_var
	var(buf, BUFFERSIZE, 1)
	var(temp,	8)	
begin_fn(main)
	/* check command line parameters */
	cmp		%i0,		2		!compare number of arguments on command line
	bne		terminate			!if they are not equal to 2, branch to terminate
	nop
	set		header,		%o0		!set the header print format into %o0
	call	printf				!and print
	nop
	/* start file open request process */
	open:
		ld		[%i1+4],	%o0		!load the file-name into %o0
		clr		%o1					!open file to read
		clr		%o2					!mode	
		mov		OPEN,		%g1		!open request
		ta		0
		bcc		open_ok				!branch to open_ok when file is opened without any errors
		mov		%o0,		%ff_r	!put file descriptor into %ff_r
		mov		1,			%g1		!error, exit
		ta		0
		
	/* get e^x from the x value read */
	read:
		ld		[%fp	+	buf],		%o1		!load the part of the buffer that is read from file into %o1
		ld		[%fp	+	buf	+	4],	%o2		!load next part into %o2
		ldd		[%fp	+	buf],	%f16		!load the x value as a double into %f16
		set		x,			%o0					!set the pointer x into %o0
		std		%f16,		[%o0]				!store x value as a double into memory
		call	eX								!call subroutine to calculate e^x
		nop
		/* print eX values */
		set		result,		%o0					!set the pointer of result (e^x) into %o0	
		ldd		[%o0],		%ex_fr				!load the double value of result (i.e. e^x) into %ex_fr
		std		%ex_fr,		[%fp	+	temp]	!store the result into temp on stack
		set		inputX,		%o0					!set string to print e^x
		ld		[%fp	+	temp],			%o3	!load part of e^x value into %o3
		ld		[%fp	+	temp	+	4],	%o4	!load the other part of the double value into %o4
		call	printf							!and print
		nop
		/* print eNegX values */
		call	eNegX							!call subroutine to calculate e^(-x)
		nop
		set	result,	%o0							!set the pointer for result (containing the value of e^(-x))
		ldd	[%o0],		%enx_fr					!load the value of e^(-x) as a double precision float into %enx_fr
		st	%f24,	[%fp	+	temp]			!store the higher part of the result into temp on stack
		st	%f25,	[%fp	+	temp	+	4]	!and the lower part ino temp + 4 on stack
		set	finalPrint,	%o0						!set the finalPrint format to %o0
		ld		[%fp	+	temp],			%o1	!load the higher part of the result into %o0
		ld		[%fp	+	temp	+	4],	%o2	!load the lower part of the result into %o2
		call	printf							!and print
		nop
	/* start file read request process */
	open_ok:
		mov		%ff_r,	%o0				!move the file descriptor into %o0
		add		%fp,	buf,	%o1		!put the pointer to buffer in %o1
		mov		BUFFERSIZE,		%o2		!number of chars to read (size of double i.e. 8)
		mov		READ,			%g1		!read request
		ta		0
		addcc	%o0,	0,		%n_r	!check if any chars have been read
		bg		read					!if chars are read, branch to read
		nop
		ba		done					!branch to done
		nop
	terminate:
		set		errorStatement,	%o0		!set pointer for errorStatement into %o0
		call	printf					!and print
		nop
	done:
		end_fn(main)

