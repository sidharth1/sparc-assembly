/* SIDHARTH JHA 10032687 CPSC 355 Assignment3 */

include(macro_defs.m)
define(SIZE,		40)	!the variable SIZE is set to 40
define(i_r,		i1)		!the counter i is set to hold the value in register %i1
define(j_r,		i2)		!the counter j is set to hold the value in register %i2
define(tmp_r,	l2)		!the variable tmp is set to hold the value in register %l2
define(s_r,		i3)


local_var
var(arr_s,	4,		4*40)			!create and array called arr_s with size of 4 bytes for each of its 40 members with a total of 200 bytes

stringA:
	.asciz "\nSorted array:\n"
	.align 4

stringB:
	.asciz "v[%-d] \t=\t %d\n"	!used for printing the unsorted & sorted arrays
	.align 4			

begin_main							!begin main
	mov		0,				%s_r
	mov		0,				%i_r		!initialize the counter i to 0 for the first loop which initializes the array

	initializeArr:						!initialize the array arr_s
		cmp		%i_r,	SIZE		!compare the counter i to 40
		be,a		insSort				!branch to insSort when i = 40
		mov		1,			%i_r		!re-initialize counter i to 1
		
		call		rand							!generate a random value into register %o0
		smul		%i_r,	4,			%l0
		mov		256,		%o1				!move 256 in register %o1 to be the second input for the modulo
		call		.rem							!find the remainder of the random number and 256 which will be between 0 and 256 and store the result in %o0
		add		%fp,		%l0,		%l0
		mov		%o0,	%g2				!move the random number (between 0 and 256) into the register %g2
		
		
		st			%g2,	[%l0	+	arr_s]	!store the random value into the array (each iteration of this loop moves this command to store in the next 'element' of the array)
		clr		%o0								!clear the input/output register %o0
		clr		%o1								!clear the input register %o1
		
		set		stringB,			%o0
		mov		%i_r,			%o1				! the first input to print --> counter value i --> v[i]
		call		printf
		mov		%g2,			%o2				!the second input to print "randVALUE" --> v[i] = "randVALUE"
		ba		initializeArr						!branch always to InitializeArr (loop)
		inc		%i_r									!increment counter i by 1 if we branch
	
	insSort:
		outerWhile:
			cmp		%i_r,				SIZE		!compare the value of the counter %i_r to the SIZE
			be		printA							!branch to print statement when i = 40
			smul		%i_r,	4,			%l0		!i = i*4 to be added to the frame pointer
			add		%fp,		%l0,		%l0		!add to the frame pointer
			ld			[%l0	+	arr_s],	%tmp_r	!load the array element into temp
			mov		%i_r,				%j_r		! j = i
			innerWhile:
				cmp		%j_r,	%s_r			!compare j to 0
				ble,a		outside						!if less than or equal to, branch outside
				inc		%i_r							!increment i by 1
				ifStatement:
					sub		%j_r,	1,			%l6						!register %l6 stores the value of j-1
					smul		%l6,		4,			%l3						!(j-1)*4 into %l3 to be added to the frame pointer
					add		%fp,		%l3,		%l3						!add %l3 to the frame pointer
					ld			[%l3	+	arr_s],	%g2						!load the array element into %g2
					clr		%l3												!clear the %l3 register
					cmp		%tmp_r,			%g2						!compare temp to the array element in %g2
					bge,a		outside										!branch outside if temp is greater than or equal to value in %g2
					inc		%i_r												!increment counter i by 1 if we branch
					smul		%j_r,				4,			%l4			!%l4 stores the value of j*4 to be added to the frame pointer
					add		%fp,					%l4,		%l4			!add to frame pointer
					st			%g2,				[%l4	+	arr_s]		!store the calue in %g2 into the array
					clr		%l4												!clear %l4 register
				ba			innerWhile				!branch back to innerWhile loop
				dec			%j_r						!decrement value in j register by 1 (j--)
		outside:
			smul		%j_r,	4,			%l5		!%l5 stores j*4
			add		%fp,		%l5,		%l5		!add %l5 to the frame pointer
			ba	outerWhile						!branch back to outerWhile loop
			st			%tmp_r,	[%l5+arr_s]	!store the value in temp into the array
				
	printA:
		set 	stringA,				%o0			!print the label for the "Sorted array:"
		call printf
	print:
		cmp	%s_r,	SIZE						!compare counter s to 40 (s begins at 0)
		be	done									!when counter s = 40 branch to done
		set stringB,				%o0			!set the print for the elements of the sorted array
		mov	%s_r,				%o1			!move counter into %o1 to symbolize the i'th value of the array element being printed
		smul	%s_r,	4,			%l5			!multiply counter by 4 to be stored into register %l5 to be added into the frame pointer
		add	%fp,		%l5,		%l5			!add to the frame pointer
		call	printf									
		ld		[%l5	+	arr_s],	%o2			!load array value into register %o2 for printing
		ba	print									!branch back to the print loop
		inc	%s_r								!increment counter s by 1
		
done:
end_main	!end main

