/* SIDHARTH JHA 10032687 CPSC 355 Assignment1 part 1 */

.global	main
main:	save	%sp,	-96,	%sp

mov	-2,	%l1		!Initialise register l1 = x
mov	0,	%l0	!Initialize l0 = -65

loop:	!begin while loop
	cmp		%l1,	10	!compares the value in %l1 and 10
	bg		done		!if value in %l1 is greater than 10, skips to done and exits loop
	nop
	
	smul	%l1,	%l1,	%l2	!x*x stored in %l2 
	smul	%l1,	%l2,	%l2	!x^3 stored in %l2
	smul	%l2,	2,		%l2 !2x^3 is stored in %l2
	
	smul	%l1,	%l1,	%l3	!x^2 stored in %l3
	smul	%l3,	-19,	%l3 !-19x^2 stored in %l3
	
	smul	%l1,	9,	%l4		!9x stored in %l4
	
	add		%l2,	%l3,	%l5		!2x^3 - 19x^2 stored in %l5
	add		%l5,	%l4,	%l5 	!value of %l5 + 9x stored in %l5	
	add		%l5,	45,		%l5		!value of %l5 + 45 stored in %l5     y = value in register l5
	
	minchk:		!next step to check if computed y value is smaller than the value in register %l0
		cmp		%l5,	%l0
		bg		notmin 		!if %l5greater than l0 then exit if statement
		nop
		mov		%l5,	%l0	!otherwise copy the value of l5 into register l0
	notmin:

	set		fmt,	%o0 !print current minimum on each iteration of the loop
	mov		%l0,	%o1
	mov		%l1,	%o2
	mov		%l5,	%o3
	call printf
	nop
	
	add	 %l1,	1,	%l1		!increment the value in %l1 by 1 OR x++
	ba	loop	!branch back to loop i.e repeat
	nop

fmt:
		.asciz	"Current minimum is: %4d, x is %4d, y is %4d\n"
		.align 4
done:
	mov		1,	%g1
	ta		0		!The final minimum is stored in register %l0 at the end of the program
	