/* SIDHARTH JHA 10032687 CPSC 355 Assignment1 part 2 */
define(x_r, l1) 			!x is assigned to register l1
define(y_r, l5) 			!y is assigned to registed l5
define(currentMin_r, l0)	!currentMin is assigned to register l0
define(xMax, 10)			!maximum domain is 10
define(xMin, -2)			!minimum domain is -2
define(a0, 2)				!constant 1
define(a1, -19)				!constant 2
define(a2, 9)				!constant 3
define(a3, 45)				!constant 4
define(a_r, l2)				!%a_r assigned 2x^3
define(b_r, l3)				!%b_r assigned -19x^2
define(c_r, l4)				!%c_r assigned 9x

.global	main
main:	save	%sp,	-96,	%sp

mov		xMin,	%x_r			!Initialise x = -2
mov		0,	%currentMin_r	!Initialize currentMin to -65
loop:	!begin while loop
	cmp		%x_r,	xMax			!compares the values of x and xMax=10
	bg		done					!if x is greater than xFinal=10, skips to done and exits the loop
	nop
	
	smul	%x_r,	%x_r,	%a_r	!x*x stored in %a_r
	smul	%x_r,	%a_r,	%a_r	!x^3 stored in %a_r
	smul	%a_r,	a0,		%a_r 	!2x^3 stored in %a_r
	
	smul	%x_r,	%x_r,	%b_r	!x^2 stored in %b_r
	smul	%b_r,	a1,		%b_r 	!-19x^2 stored in %b_r
	
	smul	%x_r,	a2,		%c_r	!9x stored in %c_r
	
	add		%a_r,	%b_r,	%y_r	!add 2x^3 + (-19x^2) and store it in y_r
	add		%y_r,	%c_r,	%y_r 	!add y_r + 9x and overwrite to y_r	
	add		%y_r,	a3,		%y_r	!add y_r + 45 and overwrite to %y_r
	
	minchk:		!next step to check if computed value in %y_r is smaller than the value in %currentMin_r
		cmp		%y_r,	%currentMin_r
		bg		notmin 					!if %y_r is greater than currentMin_r then exit if statement
		nop
		mov		%y_r,	%currentMin_r 	!otherwise copy the value in %y_r into %currentMin_r
	notmin:
	
	set		fmt,	%o0 !print current minimum along with x and y values on each iteration of the loop
	mov		%currentMin_r,	%o1
	mov		%x_r,	%o2
	call printf
	mov		%y_r,	%o3
	
	ba	loop				!branch back to loop i.e repeat
	add	%x_r,	1,	%x_r	!x++ increment the value in %x_r by 1

fmt:
		.asciz	"Current minimum is: %4d, x is %4d, y is %4d\n"
		.align 4
done:
	mov	1,	%g1
	ta	0 !register l0 contains the minimum value at the end of the program
	