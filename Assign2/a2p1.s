/* SIDHARTH JHA 10032687 CPSC 355 Assignment2 part 1 */

string:	
	.asciz "Checksum = %x \tL4= %x \tL5= %x \tL6= %x \tL7= %x \n "
	.align 4
	
.global	main
main:	save	%sp,	-96,	%sp

set	0xffff,		%l0	!set the register l0 to contain 1111 1111 1111 1111
set	0xaaaa8c01,	%l4	!set the register l4 to contain 1010 1010 1010 1010 1000 1100 0000 0001
set	0xff001234,	%l5	!set the register l5 to contain 1111 1111 0000 0000 0001 0010 0011 0100
set	0x13579bdf,	%l6	!set the register l6 to contain 0001 0011 0101 0111 1001 1011 1101 1111
set	0xc8b4ae32,	%l7	!set the register l7 to contain 1100 1000 1011 0100 1010 1110 0011 0010
set	0x1021,		%g3	!set the bitmask for the 3 XOR's
set 0x8000,		%l3 !set %l3
srl	%l3,	15,	%l3 !use to clear b0
set 0x10000,	%g4 !set %g5
mov	0,			%g6	!counter for the DataStream loop

set 	string, 	%o0			!set string for printing
		mov		%l4,		%o2
		mov		%l5,		%o3
		mov		%l6,		%o4
		mov		%l7,		%o5
		call 	printf
		mov		%l0, 		%o1			!copy l0 to o1

DataStream:
	cmp		%g6,	127
	bg		done
	set		0x8000,		%g2	!set the bitmask for the register %l0 (the checksum)
	and		%l0,	%g2,	%l1	!register %l1 stores the bit moved out of the checksum
	sll		%l0,	1,		%l0	!shift register %l0 left by 1
	bclr	%g4,	%l0			!clear b16 of register %l0
	srl		%l4,	31,		%l2	!register %l2 stores the bit for the first XOR in the lower end
	ba check
	
	B15One:
		xor		%l2,	%l0,	%l0	!set the value in b0
		xor		%l0,	%g3,	%l0	!final xor and copy into the checksum (%l0)
		ba Shift
	check:
		btst 	%l1,	%g2			!check if bit in register %l1 and the bit in register %g2 are equal
		bg		B15One				!branch to B15One if greater than zero else continue down
	
	B15Zero:
		bclr	%l3,	%l0			!clear b0 of the checksum
		xor		%l2,	%l0,	%l0	!set the value in b0
	
	
	Shift:	
		set		0x80000000,		%g2	!set the bitmask for the register %l0 (the checksum)
		sll		%l4,	1,		%l4	!shift register %l4 1 bit to the left
		and		%l5,	%g2,	%l1	!register %l1 stores the bit moved out of the register %l5
		srl		%l1,	31,		%l1
		bset	%l1,	%l4			!sets the right-most bit of register %l4 to the GSB of %l5
	
		sll		%l5,	1,		%l5	!shift %l5 by 1
		and		%l6,	%g2,	%l1
		srl		%l1,	31,		%l1
		bset	%l1,	%l5			!sets the right-most bit of register %l5 to the GSB of %l6

		sll		%l6,	1,		%l6	!shift %l5 by 1
		and		%l7,	%g2,	%l1
		srl		%l1,	31,		%l1
		bset	%l1,	%l6			!sets the right-most bit of register %l5 to the GSB of %l6
		sll		%l7,	1,		%l7
	
	Print:
		set 	string, 	%o0			!set string for printing
		mov		%l4,		%o2
		mov		%l5,		%o3
		mov		%l6,		%o4
		mov		%l7,		%o5
		call 	printf
		mov		%l0, 		%o1			!copy l0 to o1
		
		ba		DataStream				!branch back to DataStream i.e. repeat
		add		%g6,	1,	%g6			!increment the counter %g6 by 1

done:
	mov 1, %g1		
	ta 0
