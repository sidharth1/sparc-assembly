/* SIDHARTH JHA 10032687 CPSC 355 Assignment2 part 1 */

Initial_1:	
	.asciz "InitialProduct_1 = %x \tInitialMultiplier_1 = %x \tInitialMultiplicand_1 = %x\n"
	.align 4
Final1:	
	.asciz "FinalProduct_1 = %x \tFinalMultiplier_1 = %x \tFinalMultiplicand_1 = %x\n"
	.align 4
Initial_2:	
	.asciz "InitialProduct_2 = %x \tInitialMultiplier_2 = %x \tInitialMultiplicand_2 = %x\n"
	.align 4
Final2:	
	.asciz "FinalProduct_2 = %x \tFinalMultiplier_2 = %x \tFinalMultiplicand_2 = %x\n"
	.align 4
Initial_3:	
	.asciz "InitialProduct_3 = %x \tInitialMultiplier_3 = %x \tInitialMultiplicand_3 = %x\n"
	.align 4
Final3:	
	.asciz "FinalProduct_3 = %x \tFinalMultiplier_3 = %x \tFinalMultiplicand_3 = %x\n"
	.align 4
.global	main
main:	save	%sp,	-96,	%sp
/*PART1:*/
set		0x80000000,	%l3	!set the bit mask for multiplier
set		0x1,		%l4	!set the bit mask for the product
set		120490892,	%l1	!set the multiplier value in register %l1
set		82732983,	%l2	!set the multiplcand value in register %l2
mov		0,			%g3	!initialize the counter for Loop

and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
cmp		%l1,	0
bl Else
mov		0,		%g2		!register %g2 (negative) holds 0
ba While
nop
	
Else:
	mov		1,		%g2	!else %g2 (negative) holds 1
While:	
	set		0,			%l0	!initialize the product
	set 	Initial_1, 	%o0			!set string for printing
	mov		%l1,	%o2
	mov		%l2,	%o3
	call 	printf
	mov		%l0, 		%o1			!copy l0 to o1
Loop:
	cmp		%g3,	31		!compares 
	bg		L
	and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
	cmp		%l5,	0
	be,a	el
	and		%l0,	%l4,	%l5	!store the rightmost bit of the product register in %l5
	
	add		%l0,	%l2,	%l0	!product=product+multiplicand where resulting product is held in %l0
	
	el:
		sra		%l0,	1,		%l0	!shift the product register right by one bit
		srl		%l1,	1,		%l1	!shift the multiplier register right by one bit
		sll		%l5,	31,		%l5	!shift the value to be appended 31 bits left
		add		%l1,	%l5,	%l1	!add the last
		
		ba	Loop
		add		%g3,	1,		%g3	!increment the counter by 1

L:
	cmp		%g2,	0			!check if negative = 0
	be		print
	nop
	sub		%l0,	%l2,	%l0	!subtract multiplicand from product and hold result in %l0 register
print:	
	set 	Final1, 	%o0			!set string for printing
	mov		%l0, 		%o1			!copy l0 to o1
	mov		%l1,		%o2
	call 	printf
	mov		%l2,		%o3

/*PART 2:*/

set		0x80000000,	%l3	!set the bit mask for multiplier
set		0x1,		%l4	!set the bit mask for the product
set		-120490892,	%l1	!set the multiplier value in register %l1
set		82732983,	%l2	!set the multiplcand value in register %l2
mov		0,			%g3	!initialize the counter for Loop

and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
cmp		%l1,	0
bl 		Else1
mov		0,		%g2	!register %g2 (negative) holds 0
ba While1
nop
	
Else1:
	mov		1,		%g2	!else %g2 (negative) holds 1
While1:	
	set		0,			%l0	!initialize the product
	set 	Initial_2, 	%o0			!set string for printing
	mov		%l1,	%o2
	mov		%l2,	%o3
	call 	printf
	mov		%l0, 		%o1			!copy l0 to o1
Loop1:
	cmp		%g3,	31		!compares 
	bg		L1
	and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
	cmp		%l5,	0
	be,a		el1
	and		%l0,	%l4,	%l5	!store the rightmost bit of the product register in %l5
	
	add		%l0,	%l2,	%l0	!product=product+multiplicand where resulting product is held in %l0
	
	el1:
		sra		%l0,	1,		%l0	!shift the product register right by one bit
		srl		%l1,	1,		%l1	!shift the multiplier register right by one bit
		sll		%l5,	31,		%l5	!shift the value to be appended 31 bits left
		add		%l1,	%l5,	%l1	!add the last
		
		ba	Loop1
		add		%g3,	1,		%g3	!increment the counter by 1

L1:
	cmp		%g2,	0	!check if negative = 0
	be		print1
	nop
	sub		%l0,	%l2,	%l0	!subtract multiplicand from product and hold result in %l0 register
print1:	
	set 	Final2, 	%o0			!set string for printing
	mov		%l1,	%o2
	mov		%l2,	%o3
	call 	printf
	mov		%l0, 		%o1			!copy l0 to o1

/*PART3:	*/
set		0x80000000,	%l3	!set the bit mask for multiplier
set		0x1,		%l4	!set the bit mask for the product
set		-120490892,	%l1	!set the multiplier value in register %l1
set		-82732983,	%l2	!set the multiplcand value in register %l2
mov		0,			%g3	!initialize the counter for Loop

and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
cmp		%l1,	0
bl Else2
mov		0,		%g2	!register %g2 (negative) holds 0
ba While2
nop
	
Else2:
	mov		1,		%g2	!else %g2 (negative) holds 1
While2:	
	set		0,			%l0	!initialize the product
	set 	Initial_3, 	%o0			!set string for printing
	mov		%l1,	%o2
	mov		%l2,	%o3
	call 	printf
	mov		%l0, 		%o1			!copy l0 to o1
Loop2:
	cmp		%g3,	31		!compares 
	bg		L2
	and		%l1,	%l4,	%l5	!store the bit 31 of the multiplier in register %l5
	cmp		%l5,	0
	be,a	el2
	and		%l0,	%l4,	%l5	!store the rightmost bit of the product register in %l5
	
	add		%l0,	%l2,	%l0	!product=product+multiplicand where resulting product is held in %l0
	
	el2:
		sra		%l0,	1,		%l0	!shift the product register right by one bit
		srl		%l1,	1,		%l1	!shift the multiplier register right by one bit
		sll		%l5,	31,		%l5	!shift the value to be appended 31 bits left
		add		%l1,	%l5,	%l1	!add the last
		
		ba	Loop2
		add		%g3,	1,		%g3	!increment the counter by 1

L2:
	cmp		%g2,	0	!check if negative = 0
	be		print2
	nop
	sub		%l0,	%l2,	%l0	!subtract multiplicand from product and hold result in %l0 register
print2:	
	set 	Final3, 	%o0			!set string for printing
	mov		%l1,	%o2
	mov		%l2,	%o3
	call 	printf
	mov		%l0, 		%o1			!copy l0 to o1
Done:
	mov 1, %g1		
	ta 0
	
	

